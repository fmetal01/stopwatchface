package dev.felixbender.stopwatchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.*;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.view.SurfaceHolder;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class StopWatchFaceService extends CanvasWatchFaceService {

    private static final int MSG_UPDATE_TIME = 0;
    private static long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    @Override
    public Engine onCreateEngine() {
        return new StopWatchFaceEngine();
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<StopWatchFaceEngine> weakReference;

        private EngineHandler(StopWatchFaceEngine reference) {
            weakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            StopWatchFaceEngine engine = weakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class StopWatchFaceEngine extends CanvasWatchFaceService.Engine {
        // Util
        private final Handler updateTimeHandler = new EngineHandler(this);
        private Calendar calendar;
        private final BroadcastReceiver timeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                calendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        private Vibrator vibrator;
        private BatteryManager batteryManager;
        // Paints
        private Paint handPaint;
        private Paint circlePaint;
        private Paint stopWatchPaint;
        private Paint resetPaint;
        private Paint batteryPaint;
        // Colors
        private int watchHandColor;
        private int backgroundColor;
        // Coordinates
        private float centerAnalogX;
        private float centerAnalogY;
        private float centerDigitalX;
        private float centerDigitalY;
        private float centerResetX;
        private float centerResetY;
        private float centerBatteryX;
        private float centerBatteryY;
        // hand length
        private float minuteHandLength;
        private float hourHandLength;
        private float handEndCapRadius;
        private int height = 0;
        //Boolean
        private boolean registeredTimeZoneReceiver = false;
        // stopwatch
        private long stoppedTime;
        private long lastStoppedTime;
        private boolean stopWatchRunning = false;

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(StopWatchFaceService.this).setAcceptsTapEvents(true).build());
            calendar = Calendar.getInstance();

            // Background
            backgroundColor = Color.BLACK;

            // Stopwatch
            stoppedTime = 0l;
            lastStoppedTime = 0l;
            vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
            batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);

            // Watchface
            initWatchFace();
        }

        @Override
        public void onDestroy() {
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            this.height = height;

            centerAnalogX = width / 2f;
            centerAnalogY = height / 5.5f;

            centerDigitalX = width / 2f;
            centerDigitalY = height / 2f;

            centerResetX = width / 2f;
            centerResetY = height * 0.75f;

            centerBatteryX = width / 4f;
            centerBatteryY = width / 4f;

            stopWatchPaint.setTextSize(height * 0.35f);
            batteryPaint.setTextSize(height * 0.05f);

            handPaint.setStrokeWidth(width * 0.012f);
            // reset text size
            float textSize = height * 0.2f;
            resetPaint.setTextSize(textSize);
            while (resetPaint.measureText(getString(R.string.reset_text)) > width * 0.75f) { // if text to wide, reduce 'til fits
                textSize -= 1f;
                resetPaint.setTextSize(textSize);
            }

            minuteHandLength = 0.2f * centerAnalogX;
            hourHandLength = 0.15f * centerAnalogX;
            handEndCapRadius = 0.012f * height;
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            calendar.setTimeInMillis(System.currentTimeMillis());

            // Background
            canvas.drawColor(backgroundColor);
            drawWatchFace(canvas);
        }

        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case TAP_TYPE_TAP:
                    if ((float) y > (height * 0.3f) && (float) y < (height * 0.7f)) {
                        long now = System.currentTimeMillis();
                        // Check for Version for vibrate
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            vibrator.vibrate(500);
                        }

                        if (!stopWatchRunning) { // stopwatch running again
                            stopWatchRunning = true;
                            lastStoppedTime = now;
                            stopWatchPaint.setColor(Color.WHITE);
                            resetPaint.setColor(Color.GRAY);
                        } else { // stopwatch not running anymore
                            stopWatchRunning = false;
                            stoppedTime += now - lastStoppedTime;
                            stopWatchPaint.setColor(Color.RED);
                            resetPaint.setColor(Color.WHITE);
                        }
                    } else if ((float) y > (height * 0.7f)) {
                        if (!stopWatchRunning) {
                            stoppedTime = 0l;
                            lastStoppedTime = 0l;
                        }
                    }
                    break;
                default:
                    break;
            }
            invalidate();
        }

        /**
         * Updates the time
         */
        public void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                updateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);

                if (stopWatchRunning) {
                    stoppedTime += (timeMs - lastStoppedTime);
                    lastStoppedTime = timeMs;
                }
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();
                calendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            } else {
                unregisterReceiver();
            }

            updateTimer();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);

            if (inAmbientMode) {
                // watch hand
                watchHandColor = Color.BLACK;
                circlePaint.setColor(Color.BLACK);
                circlePaint.setAntiAlias(false);
                // stopwatch
                stopWatchPaint.setColor(Color.WHITE);
                stopWatchPaint.setStyle(Paint.Style.STROKE);
                stopWatchPaint.setAntiAlias(false);
                // reset
                resetPaint.setColor(Color.BLACK);
                resetPaint.setAntiAlias(false);
                // battery
                batteryPaint.setColor(Color.BLACK);
                batteryPaint.setAntiAlias(false);
            } else {
                // watch hand
                watchHandColor = Color.WHITE;
                circlePaint.setColor(Color.WHITE);
                circlePaint.setAntiAlias(true);
                // battery
                batteryPaint.setColor(Color.WHITE);
                batteryPaint.setAntiAlias(true);
                // stopwatch, reset
                stopWatchPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                if (stopWatchRunning) {
                    stopWatchPaint.setColor(Color.WHITE);
                    resetPaint.setColor(Color.GRAY);
                } else {
                    stopWatchPaint.setColor(Color.RED);
                    resetPaint.setColor(Color.WHITE);
                }
                resetPaint.setAntiAlias(true);
                stopWatchPaint.setAntiAlias(true);
            }

            onVisibilityChanged(isVisible());
        }

        private void drawWatchFace(Canvas canvas) {
            // Draw Points
            float circleRadius = centerAnalogX * 0.25f;
            for (int i = 0; i < 12; i++) {
                float rotation = (float) (i * Math.PI * 2 / 12);
                float x = (float) Math.sin(rotation) * circleRadius;
                float y = (float) -Math.cos(rotation) * circleRadius;
                canvas.drawCircle(centerAnalogX + x, centerAnalogY + y, handEndCapRadius * 0.3f, circlePaint);
            }

            // rotations for hands
            final float minuteRotation = calendar.get(Calendar.MINUTE) * 6f;
            final float hourHandOffset = calendar.get(Calendar.MINUTE) / 2f;
            final float hourRotation = (calendar.get(Calendar.HOUR) * 30f) + hourHandOffset;

            // draw analog watch
            canvas.save();
            canvas.rotate(hourRotation, centerAnalogX, centerAnalogY);
            canvas.drawLine(centerAnalogX, centerAnalogY, centerAnalogX, centerAnalogY - hourHandLength, handPaint);

            canvas.rotate(minuteRotation - hourRotation, centerAnalogX, centerAnalogY);
            canvas.drawLine(centerAnalogX, centerAnalogY, centerAnalogX, centerAnalogY - minuteHandLength, handPaint);

            canvas.drawCircle(centerAnalogX, centerAnalogY, handEndCapRadius, circlePaint);
            canvas.restore();

            // calculations for stopwatch
            if (stopWatchRunning) {
                long now = System.currentTimeMillis();
                if (stoppedTime == 0) {
                    stoppedTime++;
                } else {
                    stoppedTime += now - lastStoppedTime;
                }
                lastStoppedTime = now;
            }

            int minutes = (int) (stoppedTime / 60000) % 60;
            int seconds = (int) (stoppedTime / 1000) % 60;
            canvas.drawText(String.format("%02d", minutes) + ":" + String.format("%02d", seconds), centerDigitalX, centerDigitalY + (stopWatchPaint.getTextSize() / 3f), stopWatchPaint);

            // reset
            canvas.drawText(getString(R.string.reset_text), centerResetX, centerResetY + (resetPaint.getTextSize() / 3f), resetPaint);

            // battery
            canvas.drawText(batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY) + "%", centerBatteryX, centerBatteryY + (batteryPaint.getTextSize() / 3f), batteryPaint);
        }

        private void initWatchFace() {
            watchHandColor = Color.WHITE;

            handPaint = new Paint();
            handPaint.setColor(watchHandColor);
            handPaint.setStrokeWidth(5f);
            handPaint.setAntiAlias(true);
            handPaint.setStrokeCap(Paint.Cap.ROUND);
            handPaint.setStyle(Paint.Style.STROKE);

            circlePaint = new Paint();
            circlePaint.setColor(Color.WHITE);
            circlePaint.setAntiAlias(true);
            circlePaint.setStyle(Paint.Style.FILL_AND_STROKE);

            stopWatchPaint = new Paint();
            stopWatchPaint.setColor(Color.RED);
            stopWatchPaint.setStyle(Paint.Style.FILL_AND_STROKE);
            stopWatchPaint.setTextAlign(Paint.Align.CENTER);
            stopWatchPaint.setAntiAlias(true);

            resetPaint = new Paint();
            resetPaint.setColor(Color.WHITE);
            resetPaint.setTextAlign(Paint.Align.CENTER);
            resetPaint.setAntiAlias(true);

            batteryPaint = new Paint();
            batteryPaint.setColor(Color.WHITE);
            batteryPaint.setTextAlign(Paint.Align.CENTER);
            batteryPaint.setAntiAlias(true);
        }

        private void registerReceiver() {
            if (registeredTimeZoneReceiver)
                return;
            registeredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            StopWatchFaceService.this.registerReceiver(timeZoneReceiver, filter);
        }

        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        private void unregisterReceiver() {
            if (!registeredTimeZoneReceiver)
                return;
            registeredTimeZoneReceiver = false;
            StopWatchFaceService.this.unregisterReceiver(timeZoneReceiver);
        }

        private void updateTimer() {
            updateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning())
                updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
        }
    }
}
