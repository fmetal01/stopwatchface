# Data Protection

This Watch Face does not use any personal data.
If you have any concerns, please feel free to [contact me](mailto:felixbenderpb@gmail.com).