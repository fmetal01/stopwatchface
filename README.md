# StopWatchFace
[![pipeline status](https://gitlab.com/fmetal01/stopwatchface/badges/master/pipeline.svg)](https://gitlab.com/fmetal01/stopwatchface/commits/master)
<a href="https://play.google.com/store/apps/details?id=dev.felixbender.stopwatchface"><img alt="Get it on Google Play" src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png" height=40px /></a>

## Content

1. [Layout](#layout)
2. [Versions](#versions)
    - [Changelog](#changelog)
    - [APKs](#apks)
3. [Issues](#issues)

## Layout
This is a watchface with a stopwatch.
The face contains a small analog watch on the top showing the current time.
Next to that, the battery level of the watch is displayed.
In the middle there is the stopwatch showing minutes and seconds.
The stopwatch is able to display times up to 59 minutes and 59 seconds.
Under the stopwatch, there is a button to reset the stopwatch,
but this is only possible if the stopwatch isn't already running.

![Layout](readme/layout.png)

## Versions
Here is a list with all released versions.
Every version also contains a changelog.
Click [here](https://gitlab.com/fmetal01/stopwatchface/-/releases) for a list with all released versions.

### Changelog
- Version 2 [(latest APK)](readme/apk/v2/v2_2_1_RELEASE.apk)
  - [v2.2.1-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.2.1-RELEASE)
  - [v2.2.0-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.2.0-RELEASE)
  - [v2.1.1-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.1.1-RELEASE)
  - [v2.1.0-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.1.0-RELEASE)
  - [v2.0.1-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.0.1-RELEASE)
  - [v2.0.0-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v2.0.0-RELEASE)
- Version 1 [(latest APK)](readme/apk/v1/v1_1_0_RELEASE.apk)
  - [v1.1.0-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v1.1.0-RELEASE)
  - [v1.0.0-RELEASE](https://gitlab.com/fmetal01/stopwatchface/-/tags/v1.0.0-RELEASE)

### APKs
- Version 2
  - [v2.2.1-RELEASE](readme/apk/v2/v2_2_1_RELEASE.apk)
  - [v2.2.0-RELEASE](readme/apk/v2/v2_2_0_RELEASE.apk)
  - [v2.1.1-RELEASE](readme/apk/v2/v2_1_1_RELEASE.apk)
  - [v2.1.0-RELEASE](readme/apk/v2/v2_1_0_RELEASE.apk)
  - [v2.0.1-RELEASE](readme/apk/v2/v2_0_1-RELEASE.apk)
  - [v2.0.0-RELEASE](readme/apk/v2/v2_0_0_RELEASE.apk)
- Version 1
  - [v1.1.0-RELEASE](readme/apk/v1/v1_1_0_RELEASE.apk)
  - [v1.0.0-RELEASE](readme/apk/v1/v1_0_0_RELEASE.apk)

## Issues
If you have an issue or problem with this watchface, feel free to submit an issue [here](https://gitlab.com/fmetal01/stopwatchface/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
To see a list of all issues, click [here](https://gitlab.com/fmetal01/stopwatchface/issues).
